﻿using System;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace animal_guess_who
{
    class Program
    {
        static JObject animalData;
        static JObject remainingAnimals;

        static JObject columnData;

        static void Main(string[] args)
        {
            SetData();

            StartGame();
        }

        static void SetData()
        {
            string animalJsonData = @"{
                'animals': [
                    {  
                        'name':'Elephant',
                        'size':'Very Big',
                        'colour':'Grey',
                        'legs':'4',
                        'fur':'No',
                        'feathers':'No',
                        'tail':'Yes'
                    },
                    {
                        'name':'Lion',  
                        'size':'Medium',
                        'colour':'Yellow',
                        'legs':'4',
                        'fur':'Yes',
                        'feathers':'No',
                        'tail':'Yes'
                    },
                    {
                        'name':'Dog',  
                        'size':'Small',
                        'colour':'Brown',
                        'legs':'4',
                        'fur':'Yes',
                        'feathers':'No',
                        'tail':'Yes'
                    },
                    {
                        'name':'Cat',  
                        'size':'Small',
                        'colour':'Orange',
                        'legs':'4',
                        'fur':'Yes',
                        'feathers':'No',
                        'tail':'Yes'
                    },
                    {
                        'name':'Green Snake',  
                        'size':'Very Small',
                        'colour':'Green',
                        'legs':'0',
                        'fur':'No',
                        'feathers':'No',
                        'tail':'Yes'
                    }
                ]
            }";

            var tableJsonData = @"{
                'tables': [
                    {
                        'column':'size',
                        'question':'How big is it?'
                    },
                    {
                        'column':'colour',
                        'question':'What colour is it?'
                    },
                    {
                        'column':'legs',
                        'question':'How many legs does it have?'
                    },
                    {
                        'column':'fur',
                        'question':'Does it have fur?'
                    },
                    {
                        'column':'feathers',
                        'question':'Does it have feathers?'
                    },
                    {
                        'column':'tail',
                        'question':'Does it have a tail?'
                    }
                ]
            }";


            animalData = JObject.Parse(animalJsonData);
            remainingAnimals = animalData;

            columnData = JObject.Parse(tableJsonData);
        }

        static void StartGame()
        {
            Console.WriteLine("Welcome to the 'Guess the Animal' game!");
            Console.WriteLine("To get started, think of an animal (Don't tell me what it is)");
            Console.WriteLine("");
            Console.WriteLine("To answer the question, just enter the corresponding number, then hit enter.");
            Console.WriteLine("");
            Console.WriteLine("Now let's begin!");

            AskQuestions();
        }

        static void AskQuestions()
        {
            foreach (var column in columnData["tables"])
            {
                string options = GetOptions(column);
                Console.WriteLine("");
                Console.WriteLine(column["question"]);
                Console.WriteLine(options);
                bool valid = false;
                string answer = "";
                while (!valid)
                {
                    answer = Console.ReadLine();
                    valid = ValidAnswer(answer, options);
                    if (!valid)
                    {
                        Console.WriteLine("Invalid answer. Please try again.");
                        Console.WriteLine("");
                        Console.WriteLine(column["question"]);
                        Console.WriteLine(options);
                    }
                }

                EvaluateAnswer(answer, options, column);
                string gameOver = CheckAnswer();
                if (gameOver.Length > 0)
                {
                    EndGame(gameOver);
                    break;
                }
            }
        }


        static string GetOptions(JToken column)
        {
            string options = "";
            List<string> alreadyUsed = new List<string>();
            int lineCounter = 1;
            foreach (var animal in remainingAnimals["animals"])
            {
                string question = column["column"].ToString();

                int found = alreadyUsed.IndexOf(animal[question].ToString());
                if (found == -1)
                {
                    options += lineCounter + ". " + animal[question] + '\n';
                    alreadyUsed.Add(animal[question].ToString());
                    lineCounter++;
                }
            }

            return options;
        }

        static bool ValidAnswer(string answer, string options)
        {
            string[] allAnswers = options.Split('\n');
            foreach (var option in allAnswers)
            {
                string[] splitStr = option.Split('.');
                string number = splitStr[0];
                if (number == answer)
                {
                    return true;
                }

            }
            return false;
        }

        static void EvaluateAnswer(string answer, string options, JToken column)
        {
            string[] allAnswers = options.Split('\n');
            foreach (var option in allAnswers)
            {
                string[] splitStr = option.Split('.');
                string number = splitStr[0];
                if (number == answer)
                {
                    ReselectAnimals(splitStr[1].Trim(), column);
                }

            }
        }

        static void ReselectAnimals(string answer, JToken column)
        {
            string question = column["column"].ToString();
            for (var i = ((JArray)remainingAnimals["animals"]).Count - 1; i >= 0; i--)
            {
                var animal = remainingAnimals["animals"][i];
                if (animal[question].ToString() != answer)
                {
                    remainingAnimals["animals"][i].Remove();
                }
            }
        }

        static string CheckAnswer()
        {
            string answer = "";
            if (((JArray)remainingAnimals["animals"]).Count == 1)
            {
                answer = remainingAnimals["animals"][0]["name"].ToString();
            }
            return answer;
        }

        static void EndGame(string finalAnswer)
        {
            Console.WriteLine("");
            Console.WriteLine("I believe the answer is...");
            Console.WriteLine(finalAnswer);
            Console.WriteLine("");

            Console.WriteLine("Would you like to play again?");
            Console.WriteLine("1. Yes");
            Console.WriteLine("2. No");
            var newGameResponse = Console.ReadLine();

            if (newGameResponse == "1")
            {
                SetData();

                StartGame();
            }
        }
    }
}
